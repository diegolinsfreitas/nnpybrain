__author__ = 'diego.freitas'

from pandas import DataFrame
import datetime

class Agent:
    better_company = None
    better_company_return = None
    selected_company_data = None
    initial_money = 10000
    money = None
    history = DataFrame()

    def __init__(self, nn, stock_history):
        self.nn = nn
        self.stock_history = stock_history
        self.money = self.initial_money


    def select_stocks(self, day, companies):
        self.better_company = None
        self.better_company_return = None
        self.selected_company_data = None
        for company in companies:
            company_data_current_day = self.stock_history[
                (self.stock_history.CODNEG == company) & (self.stock_history.DATA == day)]
            if company_data_current_day.empty:  # no activity at bovespa(weekend or holiday) TODO validate valid date
                break

            company_data_row = company_data_current_day.iloc[0]
            # predict tomorrow return for the company
            _return = self.nn.activate(self._get_features(company_data_row, self.nn.features))[0]
            print " Stock %s with return of %f" % (company, _return)
            #choose the best stock return
            if self.better_company is None:
                self.better_company = company
                self.better_company_return = _return
                self.selected_company_data = company_data_row
            elif _return > self.better_company_return:
                self.better_company = company
                self.better_company_return = _return
                self.selected_company_data = company_data_row

        if self.selected_company_data is None or self.selected_company_data.empty:
            return None

        print 'Hot stock for next Day %s with return of %f' % (self.better_company, self.better_company_return)
        start_date = datetime.datetime.strptime(str(day),"%Y%m%d")
        strDate = start_date.strftime("%Y-%m-%d")
        self.history = self.history.append([
            {'day': strDate, 'stock': self.better_company, 'predicted_next_return': self.better_company_return,
             'real_next_return': self.selected_company_data['TR'], 'start_money': self.money}])

        return self.better_company

    def buy(self):

        open_price = self.selected_company_data['TOMORROW_PREABE']
        self.day_trade_cost = self.get_trade_cost(self.money)
        self.bought_stocks = self.money / open_price
        print "Buying %s at %f, stocks : %f" % (self.selected_company_data["CODNEG"], open_price, self.bought_stocks)


    def sell(self):
        previous_money= self.money
        self.money = self.bought_stocks * self.selected_company_data['TOMORROW_PREULT']
        self.money -= self.day_trade_cost
        print "selling %s at %f, daytrade %f - Total: %f, profit %f" % (self.selected_company_data["CODNEG"], self.selected_company_data['TOMORROW_PREULT'], self.day_trade_cost, self.money, self.money - previous_money)
        self.day_trade_cost = 0


    def get_trade_cost(self, order):
        custodia_dia = 0.4454
        corretagem = 20.62
        emolumento = self.calc_percent(0.004842, order)
        liquidacao =  self.calc_percent(0.0275, order)
        return corretagem + emolumento + liquidacao + custodia_dia

    def calc_percent(self,tax, value):
        return ( tax * value ) / 100

    def _get_features(self, row, features_label):
        features = []
        for column in features_label:
            features.append(row[column])
        return features

    def get_growth_rate(self):
        return ((self.money - self.initial_money)/ self.initial_money) * 100