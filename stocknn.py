from pybrain.datasets import SupervisedDataSet
from pybrain.structure.networks import recurrent
from pybrain.tools.shortcuts import buildNetwork
from pybrain.tools.xml import NetworkWriter, NetworkReader

__author__ = 'diego.freitas'


from pybrain.structure import *
from pybrain.supervised.trainers import BackpropTrainer



class NeuralNetwork:

    n = None
    features = None
    targets = None

    def __init__(self, features=None, targets=None):
        self.features = features
        self.targets = targets
        if features is not None or targets is not None:
            self.n = buildNetwork(len(features), len(features), 1, recurrent=True, hiddenclass=TanhLayer , bias=True)

    def train(self,cycles, dataframe):
        dataset = SupervisedDataSet(len(self.features), 1)
        for index, row in dataframe.iterrows():
            features = []
            for label in self.features:
                features.append(row[label])
            dataset.addSample(features,row[self.targets])
        TrainDS, TestDS = dataset.splitWithProportion(0.9)
        trainer = BackpropTrainer(self.n, TrainDS, verbose=True)
        for i in xrange(cycles):
            trainer.trainEpochs(1)
        #trainer.trainUntilConvergence()
        trainer.testOnData(TestDS, True)

    def activate(self,features):
        return self.n.activate(features)

    def save(self):
       NetworkWriter.writeToFile(self.n, 'neuralnet.xml')

    def load(self, filename = 'neuralnet.xml'):
        self.n = NetworkReader.readFrom(filename)
