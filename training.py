from ggplot import ggplot, aes, geom_line, stat_smooth, ggsave, scale_x_date, date_breaks
from agent import Agent

__author__ = 'diego.freitas'

from datawraling import DataWragling
import pandas
import numpy as np
from stocknn import *
import datetime


class TraningModule:

    dataw = DataWragling()

    slice_period = 20140102

    def __init__(self):
        self.dataw.prepare_data_training()
        self.bovespadf = self.dataw.bovespadf

    def train(self, cycles):
        print "clean and prepare data"

        target_label = 'NR'

        trainingdf = self.bovespadf[(self.bovespadf.DATA <= self.slice_period)]
        #trainingdf = trainingdf[(trainingdf.DATA > 20140501)]
        comparedf = self.bovespadf[(self.bovespadf.DATA > self.slice_period)]

        print 'training'
        start = datetime.datetime.now().replace(microsecond=0)

        nn = NeuralNetwork(self.dataw.main_features_label, target_label)
        nn.train(cycles, trainingdf)
        nn.save()

        end = datetime.datetime.now().replace(microsecond=0)

        print "Total Training time %s" % (end - start)

        self.benchmarking(nn, comparedf)

    def simulate(self):
        target_label = 'TOMORROW_RETURN'
        comparedf = self.bovespadf[(self.bovespadf.DATA > self.slice_period)]
        nn = NeuralNetwork(self.dataw.main_features_label, target_label)
        nn.load()
        self.benchmarking(nn, comparedf)

    def benchmarking(self, nn, comparedf):

        companies = set(comparedf.CODNEG.values)
        agent = Agent(nn, comparedf)

        start_date = datetime.datetime.strptime(str(self.slice_period),"%Y%m%d") + datetime.timedelta(days=1)
        training_days = datetime.datetime.now()-start_date
        end_date = start_date + datetime.timedelta(days=training_days.days)
        print "Benchmarking Period %s - %s" % (start_date, end_date)
        days = 0
        #analyse a period of time the investiment
        while start_date <= end_date:
            current_day = int(start_date.strftime("%Y%m%d"))

            selected_companies = agent.select_stocks(current_day, companies)
            start_date += datetime.timedelta(days=1)
            if selected_companies is not None:
                days += 1
                agent.buy()
                agent.sell()

        print "Total money %f" % agent.money
        print agent.history
        print "R^2 : %f" % compute_r_squared(agent.history['real_next_return'], agent.history['predicted_next_return'])
        print "Growth rate %f" % agent.get_growth_rate()
        print "Average Growth rate %f" % get_average_growth_rate(days, agent.initial_money, agent.money)
        agent.history.day = pandas.to_datetime(agent.history.day)
        ggsave( filename="result", plot=ggplot(aes(x='day', y='start_money'), data=agent.history) + stat_smooth(colour="blue", se=False) + geom_line() + scale_x_date(breaks=date_breaks('1 month'), labels='%m/%y'))

def compute_r_squared(data, predictions):
    mean = np.mean(data)
    r_squared = 1 - (np.sum(np.square(data - predictions)) / np.sum(np.square(data - mean)))

    return r_squared


def normalize_features(array):
    array_normalized = (array - array.mean()) / array.std()
    return array_normalized

def get_average_growth_rate(n, past_value, present_value):
    return_ = present_value - float(past_value)
    print n, past_value, present_value, return_
    return (return_ ** (1/float(n))-1) * 100
