from agent import Agent
from stocknn import NeuralNetwork

__author__ = 'diego.freitas'


import cmd
from training import TraningModule
from datawraling import DataWragling
import urllib
import zipfile, httplib

class MoneyMachineCmd(cmd.Cmd):
    """Simple command processor example."""

    trainin_module = None
    prompt = 'prompt: '
    intro = "MoneyMachine"
    data_module = DataWragling()

    def do_cvt(self, line="stock_history_raw.txt"):
        self.data_module.bovespa_to_csv(line)

    def do_train(self, cycles):
        if self.trainin_module is None:
            self.trainin_module = TraningModule()
        if len(cycles) == 0:
            cycles = '0'
        self.trainin_module.train(int(cycles))


    def do_simul(self, line):
        if self.trainin_module is None:
            self.trainin_module = TraningModule()
        self.trainin_module.simulate()


    def do_join(self, line):
        self.data_module.join_stock_history(line.split(" "))

    def do_dw(self, line):
        if len(line) == 0:
            line = "COTAHIST_A2010.TXT COTAHIST_A2011.TXT COTAHIST_A2012.TXT COTAHIST_A2013.TXT COTAHIST_A2014.TXT"
        self.data_module.join_stock_history(line.split(" "))
        self.data_module.bovespa_to_csv()


    def do_anl(self,day_before="20141105"):
        self.data_module.load_dataframe()
        stocks = self.data_module.get_stocks()
        stock_data, features = self.data_module.get_stocks_data_for_day(int(day_before))
        print stock_data, features
        nn = NeuralNetwork(features)
        nn.load()
        agent = Agent(nn, stock_data)
        agent.select_stocks(int(day_before),stocks)
        #agent.buy()
        #agent.sell()

    def do_dc(self,line):
        urlOpener = urllib.URLopener()
        httplib.HTTPConnection.debuglevel = 1
        urlOpener.retrieve("http://www.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2014.ZIP", "COTAHIST_A2014.ZIP")
        with zipfile.ZipFile("COTAHIST_A2014.ZIP") as zf:
            zf.extractall()

    def do_q(self, line):
        return True

if __name__ == '__main__':
    MoneyMachineCmd().cmdloop()