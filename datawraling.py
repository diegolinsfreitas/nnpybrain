import datetime
from pandas import DataFrame,Series
#from ggplot import *
from agent import Agent
from stocknn import *
from pybrain.datasets import SupervisedDataSet
import datetime
from time import  strftime
import numpy as np
import pandas

__author__ = 'diego.freitas'

def toFloat(value):
        return float(value.lstrip())/100

def get_day_week(date):
    return float(datetime.datetime.strptime(str(date), '%Y%m%d').date().isoweekday())


class DataWragling:

    base_features =  ['L4R','L3R','L2R', 'L1R', 'TR']

    dummies_frame = None

    bovespadf = None

    def __init__(self):
        self.stocks_names = [
             'ABEV3',
             'BBAS3',
             'BBDC3',
             'BBDC4',
             'BRFS3',
             'BVMF3',
             'CIEL3',
             'ITSA4',
             'ITUB4',
             'PCAR4',
             'PETR3',
             'PETR4',
             'UGPA3',
             'VALE3',
             'VALE5'

        ]
        '''
        ['PETR4' 'VALE5' 'ITUB4' 'ABEV3' 'PETR3' 'ITSA4' 'BVMF3' 'BBDC4' 'BBAS3'
 'GGBR4' 'OIBR4' 'BBSE3' 'USIM5' 'VALE3' 'CCRO3' 'CSNA3' 'CMIG4' 'JBSS3'
 'SUZB5' 'TIMP3' 'CIEL3' 'MRVE3' 'ESTC3' 'KROT3' 'BRML3' 'AEDU3' 'CYRE3'
 'PDGR3' 'SANB11' 'BRFS3' 'ALLL3' 'RLOG3' 'HYPE3' 'GFSA3' 'ENBR3' 'LAME4'
 'EMBR3' 'RSID3' 'QUAL3' 'VALEB31' 'SBSP3' 'UGPA3' 'POMO4' 'BRKM5' 'EVEN3'
 'CTIP3' 'BRAP4' 'FIBR3' 'DTEX3' 'CRUZ3' 'VALEB30' 'CPFE3' 'MRFG3' 'ELET3'
 'BRPR3' 'BOVA11' 'ELET6' 'HGTX3' 'PETRH20' 'ECOR3' 'CSAN3' 'VIVT4' 'BBDC3'
 'NATU3' 'GOAU4' 'PETRI22' 'PETRA16' 'KLBN11' 'LLXL3' 'VALEA32' 'RENT3'
 'VALEA30' 'CESP6' 'PETRB14' 'GOLL4' 'VALEB32' 'VALEA31' 'PETRB15' 'PCAR4'
 'TBLE3' 'VALED28' 'VALEE28' 'LREN3' 'KLBN4' 'LIGT3' 'VALED29' 'CPLE6'
 'PETRK23' 'BRSR6' 'ODPV3' 'RAPT4' 'HRTP3' 'RADL3' 'VALEA33' 'PETRD16'
 'PETRJ74' 'SULA11' 'PETRA17' 'VALEG28' 'PETRH21']
 '''
        self.stocks_names.sort()

    def join_stock_history(self,file_names):
        with open('stock_history_raw.txt','w') as outfile:
             for fname in file_names:
                with open(fname) as infile:
                    for line in infile:
                        if(line.startswith('99') or line.startswith('00') or len(line) == 0):
                            continue
                        outfile.write(line)


    def bovespa_to_csv(self, file= "stock_history_raw.txt", CODNEG=None):
        stock_history_file = "stock_hitory.csv"
        with open(file,'r') as cotahist:
            line = "DATA,DIA,CODNEG,NOMRES,PREABE,PREULT,TOTNEG\n"
            date = None

            with open(stock_history_file,'w') as output:
                output.write(line)
                for line in cotahist:
                    new_date = line[2:10]
                    if(date != new_date):
                        date = new_date

                    output.write("%s,%s,%s,%s,%f,%f,%d\n" % (line[2:10],line[8:10],line[12:24].strip(),line[27:39],toFloat(line[56:69]),toFloat(line[108:121]),int(line[147:152].lstrip())))
                    ''' bovespadf = bovespadf.append([{
                                                      #"TIPREG": line[0:2],
                                                      "DATA": line[2:10],
                                                      "DIA": line[8:10],
                                                      #"CODBDI": line[10:12],
                                                      "CODNEG": line[12:24].strip(),
                                                      #"TPMERC": line[24:27],
                                                      "NOMRES": line[27:39],
                                                      #"ESPECI": line[39:49],
                                                      #"PRAZOT": line[49:52],
                                                      #"MODREF": line[52:56],
                                                      "PREABE": toFloat(line[56:69]),
                                                      #"PREMAX": toFloat(line[69:82]),
                                                      #"PREMIN": toFloat(line[82:95]),
                                                      #"PREMED": toFloat(line[95:108]),
                                                      "PREULT": toFloat(line[108:121])
                                                      #"PREOFC": toFloat(line[121:134]),
                                                      #"PREOFV": toFloat(line[134:147]),
                                                      #"TOTNEG": line[147:152],
                                                      #"QUATOT": line[152:170],
                                                      #"VOLTOT": line[170:188],
                                                      #"PREEXE": line[188:201],
                                                      #"INDOPC": line[201:202],
                                                      #"DATVEN": line[202:210],
                                                      #"FATCOT": line[210:217],
                                                      #"PTOEXE": line[217:230],
                                                      #"CODISI": line[230:242],
                                                      #"DISMES": line[242:245]
                                                  }])'''


                output.close()
            cotahist.close()
            #self.save_top_stocks_history(stock_history_file)


    def save_top_stocks_history(self, stocks_history="stock_hitory.csv"):
        stock_history = pandas.read_csv(stocks_history)
        codnegby =  stock_history.groupby("CODNEG")
        codnegby_mean = codnegby['TOTNEG'].mean()
        codnegby_mean.sort(ascending=False)
        top_stocks = codnegby_mean.index.values[:100]
        print top_stocks
        top_stock_history = stock_history[(stock_history.CODNEG.isin(top_stocks))]
        top_stock_history.to_csv('top_stock_hitory.csv')

    def load_dataframe(self, file='stock_hitory.csv'):
        print 'loading df'
        if self.bovespadf is None:
            self.bovespadf = pandas.read_csv(file)


            #features selection
            self.bovespadf = self.bovespadf[["DIA","DATA","CODNEG","NOMRES","PREABE", "PREULT"]]
            #self.bovespadf['DAY_OF_WEEK'] = self.bovespadf.DATA.apply(get_day_week)
            self.bovespadf['TR'] =  self.bovespadf["PREULT"] - self.bovespadf['PREABE']

            self.bovespadf['L9R'] = self.bovespadf.TR.shift(9)
            self.bovespadf['L8R'] = self.bovespadf.TR.shift(8)
            self.bovespadf['L7R'] = self.bovespadf.TR.shift(7)
            self.bovespadf['L6R'] = self.bovespadf.TR.shift(6)
            self.bovespadf['L5R'] = self.bovespadf.TR.shift(5)
            self.bovespadf['L4R'] = self.bovespadf.TR.shift(4)
            self.bovespadf['L3R'] = self.bovespadf.TR.shift(3)
            self.bovespadf['L2R'] = self.bovespadf.TR.shift(2)
            self.bovespadf['L1R'] = self.bovespadf.TR.shift(1)
            self.bovespadf['NR'] = self.bovespadf.TR.shift(-1)
            self.bovespadf['TOMORROW_PREULT'] = self.bovespadf["PREULT"].shift(-1)
            self.bovespadf['TOMORROW_PREABE'] =self.bovespadf['PREABE'].shift(-1)

            self.bovespadf.dropna()


             #reduce the sample
            self.bovespadf = self.bovespadf[(self.bovespadf.CODNEG.isin( self.stocks_names) )]

            self.bovespadf = self.bovespadf.sort(['CODNEG', 'DATA'])

            print self.bovespadf.tail()

            #self.bovespadf = self.bovespadf.sort(['CODNEG', 'DATA'])

    def get_dummies(self,dataframe):
        dummy_codnegs = pandas.get_dummies(dataframe['CODNEG'], prefix='CN')
        dummy_codnegs_header = dummy_codnegs.columns.values
        return dummy_codnegs_header, dummy_codnegs

    def prepare_data_training(self, file='stock_hitory.csv'):
        self.load_dataframe(file)

        dummy_codnegs_header, dummy_codnegs = self.get_dummies(self.bovespadf)

        #save CODNEG and the corresponding features that idenfies the company
        '''codneg_map = DataFrame()
        for stock_name in self.stocks_names:
             codneg_map = codneg_map.append([self.bovespadf[(self.bovespadf.CODNEG == stock_name)].iloc[0][dummy_codnegs_header]])

        codneg_map['CODNEG'] = self.stocks_names
        codneg_map = codneg_map.set_index(['CODNEG'])
        codneg_map.to_csv('codneg_map.csv')'''


        self.bovespadf = self.bovespadf.join(dummy_codnegs)

        print "prepare data training"
        print self.bovespadf.head()

        self.build_features_header(dummy_codnegs_header)
            
    
    def build_features_header(self, dummy_codnegs_header):
        self.main_features_label = self.base_features

        for header in dummy_codnegs_header:
            self.main_features_label.append(header)
        print "load features"
        print self.main_features_label
        return self.main_features_label

    def get_stocks(self):
       return self.stocks_names


    def get_stocks_data_for_day(self,current_day):
        print "get_stocks_data_for_day"
        stocks = self.get_stocks()

        stock_data = self.bovespadf[ (self.bovespadf.DATA == current_day)]

        dummy_codnegs_header, dummy_codnegs = self.get_dummies(self.bovespadf)

        features = self.build_features_header(dummy_codnegs_header)
        dummy_codnegs = pandas.get_dummies(stock_data['CODNEG'], prefix='CN')
        print stock_data
        stock_data = stock_data.join(dummy_codnegs)
        print stock_data
        return stock_data, features
